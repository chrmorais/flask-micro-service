# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.config.basic import BasicConfig


class TestingConfig(BasicConfig):
    """
    Testing configuration
    """
    TESTING = True
    SQLALCHEMY_BINDS = {
        'test': 'sqlite:///:memory:'
    }
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_RECORD_QUERIES = True
    SQLALCHEMY_COMMIT_ON_TEARDOWN = False
