# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os


class BasicConfig(object):
    """
    Basic configuration
    """
    ROOT_PATH = os.path.abspath(os.path.dirname(__file__))

    HOST = '127.0.0.1'
    PORT = 5000
    SERVER_NAME = '{host}:{port}'.format(host=HOST, port=PORT)

    SECRET_KEY = os.environ.get('SECRET_KEY', 'Secret')

    PASSWORD_HASH_METHOD = 'pbkdf2:sha1:1024'
    PASSWORD_SALT_LENGTH = 8

    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
    SQLALCHEMY_BINDS = {}

    @classmethod
    def init_app(cls, app):
        """
        Calls application configuration method.

        :param app: Flask application instance
        """
        app.config.from_object(cls)

    def __init__(self, app=None):
        """
        Constructor.
        Application can be configured just after config creation.

        :param app: Flask application instance
        """
        if app:
            self.init_app(app)
