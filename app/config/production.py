# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.config.basic import BasicConfig


class ProductionConfig(BasicConfig):
    """
    Production configuration
    """
    DEBUG = False
    TESTING = False
    SSL_DISABLE = False
    PORT = 443
