# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.utils import singleton
from app.config.development import DevelopmentConfig
from app.config.production import ProductionConfig
from app.config.testing import TestingConfig


CONFIG_DEVELOPMENT = 'development'
CONFIG_TESTING = 'testing'
CONFIG_PRODUCTION = 'production'


@singleton
class ConfigFactory(object):
    """
    Configuration's factory
    """
    CONFIGS = {
        CONFIG_DEVELOPMENT: DevelopmentConfig,
        CONFIG_TESTING: TestingConfig,
        CONFIG_PRODUCTION: ProductionConfig
    }

    def __call__(self, app=None, name=None):
        """
        Fabric method

        :param app: Instance of Flask application, by default is None
        :param name: Configuration name
        :return: Instance of configuration class,
                 by default returns development configuration
        """
        return self.CONFIGS.get(name, DevelopmentConfig)(app)


Config = ConfigFactory()
