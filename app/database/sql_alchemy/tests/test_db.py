# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest
import os

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from app.database.sql_alchemy import db


class TestDatabase(unittest.TestCase):

    def test_is_valid(self):
        self.assertIsInstance(db, SQLAlchemy)

    def test_has_migrations(self):
        self.assertTrue(hasattr(db, 'migrations'))

    def test_migrations_are_valid(self):
        self.assertIsInstance(db.migrations, Migrate)

    def test_migrations_directory(self):
        directory = os.path.abspath(os.path.join(
            os.path.dirname(__file__),
            os.pardir,
            'migrations'
        ))
        self.assertEqual(db.migrations.directory, directory)


if __name__ == '__main__':
    unittest.main()
