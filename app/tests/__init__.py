# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from app import Application
from app.common.utils import AssertsMixin


class TestCase(AssertsMixin, unittest.TestCase):
    """
    Basic TestCase class
    """

    app = None
    app_context = None

    @classmethod
    def setUpClass(cls):
        cls.app = Application('testing')
        cls.app_context = cls.app.app_context()
        cls.app_context.push()
        cls.client = cls.app.test_client()

    @classmethod
    def tearDownClass(cls):
        cls.app_context.pop()
