# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest
from mock import patch, MagicMock
from werkzeug.exceptions import BadRequest

from app.common.utils import patch_multiple, AssertsMixin
from app.common.api import BaseResource
from app.common.serializers.exceptions import LoadError
from app.common.services.storage_exceptions import DoesNotExist, AlreadyExists


class MyResource(BaseResource):
    pass


class TestBaseResource(AssertsMixin, unittest.TestCase):
    """
    Test BaseResource in isolation mode
    """

    def test_get_endpoint(self):
        self.assertEqual('my_resource', MyResource.get_endpoint())

    def test_has_attr_get(self):
        self.assertTrue(hasattr(BaseResource, 'get'))

    def test_has_attr_post(self):
        self.assertTrue(hasattr(BaseResource, 'post'))

    def test_has_attr_put(self):
        self.assertTrue(hasattr(BaseResource, 'put'))

    def test_has_attr_patch(self):
        self.assertTrue(hasattr(BaseResource, 'patch'))

    def test_has_attr_delete(self):
        self.assertTrue(hasattr(BaseResource, 'delete'))

    def test_serializer_type_validation(self):

        class BadSerializer(object):
            pass

        class BadResource(BaseResource):
            SERIALIZER = BadSerializer

        with self.assertRaisesDetails(TypeError, BadResource) as e:
            self.assertEqual('Not a serializer:', e.args[0])
            self.assertEqual(BadSerializer, e.args[1])

    def test_filter_serializer_type_validation(self):

        class BadFilterSerializer(object):
            pass

        class BadResource(BaseResource):
            FILTER_SERIALIZER = BadFilterSerializer

        with self.assertRaisesDetails(TypeError, BadResource) as e:
            self.assertEqual('Not a filter serializer:', e.args[0])
            self.assertEqual(BadFilterSerializer, e.args[1])

    def test_storage_type_validation(self):

        class BadStorage(object):
            pass

        class BadResource(BaseResource):
            STORAGE = BadStorage

        with self.assertRaisesDetails(TypeError, BadResource) as e:
            self.assertEqual('Not a storage:', e.args[0])
            self.assertEqual(BadStorage, e.args[1])

    def test_get_does_not_exist(self):
        with patch(
            'app.common.services.base_storage.BaseStorage.read',
            side_effect=DoesNotExist(1)
        ):
            response, code = MyResource().get(1)
            self.assertEqual({'error': DoesNotExist.description}, response)
            self.assertEqual(404, code)

    def test_get_one(self):
        with patch_multiple([
            'app.common.services.base_storage.BaseStorage.read',
            'app.common.serializers.base_serializer.BaseSerializer.dump_data'
        ]) as (m_read, m_dump_data):
            storage_data = m_read.return_value = MagicMock()
            dumped_data = m_dump_data.return_value = {'id': 1, 'a': 1, 'b': 2}
            response, code = MyResource().get(1)
            m_read.assert_called_with(1)
            m_dump_data.assert_called_with(storage_data)
            self.assertEqual(response, dumped_data)
            self.assertEqual(code, 200)

    def test_get_many(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.services.base_storage.BaseStorage.read',
            'app.common.services.base_storage.BaseStorage.count',
            'app.common.serializers.base_serializer.BaseSerializer.dump_data'
        ]) as (m_request, m_read, m_count, m_dump_data):
            m_request.args.to_dict.return_value = {}
            storage_data = m_read.return_value = [
                MagicMock(), MagicMock(), MagicMock()
            ]
            m_count.return_value = 3
            dumped_data = m_dump_data.return_value = [
                {'id': 1}, {'id': 2}, {'id': 3}
            ]
            response, code = MyResource().get()
            m_read.assert_called_with(
                filter={},
                paging={'limit': 1024, 'offset': 0})
            m_count.assert_called_with(filter={})
            m_dump_data.assert_called_with(storage_data)
            self.assertEqual({
                'filter': {},
                'paging': {'limit': 1024, 'offset': 0, 'total': 3},
                'data': dumped_data
            }, response)
            self.assertEqual(code, 200)

    def test_get_many_paging(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.services.base_storage.BaseStorage.read',
            'app.common.services.base_storage.BaseStorage.count',
            'app.common.serializers.base_serializer.BaseSerializer.dump_data'
        ]) as (m_request, m_read, m_count, m_dump_data):
            m_request.args.to_dict.return_value = {'limit': 2, 'offset': 2}
            storage_data = m_read.return_value = [MagicMock()]
            m_count.return_value = 3
            dumped_data = m_dump_data.return_value = [{'id': 3}]
            response, code = MyResource().get()
            m_read.assert_called_with(
                filter={},
                paging={'limit': 2, 'offset': 2}
            )
            m_count.assert_called_with(filter={})
            m_dump_data.assert_called_with(storage_data)
            self.assertEqual({
                'filter': {},
                'paging': {'limit': 2, 'offset': 2, 'total': 3},
                'data': dumped_data
            }, response)
            self.assertEqual(code, 200)

    def test_post_bad_request(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data'
        ]) as mocked:
            mocked[-1].side_effect = BadRequest
            response, code = MyResource().post()
            self.assertEqual({'error': BadRequest.description}, response)
            self.assertEqual(400, code)

    def test_post_load_error(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data'
        ]) as mocked:
            errors = [{'error': 'Test error'}]
            mocked[-1].side_effect = LoadError(errors)
            response, code = MyResource().post()
            self.assertEqual(errors, response)
            self.assertEqual(400, code)

    def test_post_already_exists(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data',
            'app.common.services.base_storage.BaseStorage.create',
            'app.common.services.base_storage.BaseStorage.commit'
        ]) as mocked:
            mocked[-1].side_effect = AlreadyExists
            response, code = MyResource().post()
            self.assertEqual({'error': AlreadyExists.description}, response)
            self.assertEqual(409, code)

    def test_post(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data',
            'app.common.services.base_storage.BaseStorage.create',
            'app.common.services.base_storage.BaseStorage.commit',
            'app.common.serializers.base_serializer.BaseSerializer.dump_data'
        ]) as (m_request, m_load_data, m_create, m_commit, m_dump_data):
            data = m_request.get_json.return_value = {'a': 1, 'b': 2, 'c': 3}
            validated_data = m_load_data.return_value = {'a': 1, 'b': 2}
            storage_data = m_create.return_value = MagicMock()
            dumped_data = m_dump_data.return_value = {'id': 1, 'a': 1, 'b': 2}
            response, code = MyResource().post()
            m_load_data.assert_called_with(partial=False, **data)
            m_create.assert_called_with(**validated_data)
            m_commit.assert_called_with()
            m_dump_data.assert_called_with(storage_data)
            self.assertEqual(response, dumped_data)
            self.assertEqual(code, 201)

    def test_put_bad_request(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data'
        ]) as mocked:
            mocked[-1].side_effect = BadRequest
            response, code = MyResource().put(1)
            self.assertEqual({'error': BadRequest.description}, response)
            self.assertEqual(400, code)

    def test_put_load_error(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data'
        ]) as mocked:
            errors = [{'error': 'Test error'}]
            mocked[-1].side_effect = LoadError(errors)
            response, code = MyResource().put(1)
            self.assertEqual(errors, response)
            self.assertEqual(400, code)

    def test_put_does_not_exist(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data',
            'app.common.services.base_storage.BaseStorage.update'
        ]) as mocked:
            mocked[-1].side_effect = DoesNotExist(1)
            response, code = MyResource().put(1)
            self.assertEqual({'error': DoesNotExist.description}, response)
            self.assertEqual(404, code)

    def test_put_already_exists(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data',
            'app.common.services.base_storage.BaseStorage.update',
            'app.common.services.base_storage.BaseStorage.commit'
        ]) as mocked:
            mocked[-1].side_effect = AlreadyExists
            response, code = MyResource().put(1)
            self.assertEqual({'error': AlreadyExists.description}, response)
            self.assertEqual(409, code)

    def test_put(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data',
            'app.common.services.base_storage.BaseStorage.update',
            'app.common.services.base_storage.BaseStorage.commit',
            'app.common.serializers.base_serializer.BaseSerializer.dump_data'
        ]) as (m_request, m_load_data, m_create, m_commit, m_dump_data):
            data = m_request.get_json.return_value = {'a': 1, 'b': 2, 'c': 3}
            validated_data = m_load_data.return_value = {'a': 1, 'b': 2}
            storage_data = m_create.return_value = MagicMock()
            dumped_data = m_dump_data.return_value = {'id': 1, 'a': 1, 'b': 2}
            response, code = MyResource().put(1)
            m_load_data.assert_called_with(partial=False, id=1, **data)
            m_create.assert_called_with(**validated_data)
            m_commit.assert_called_with()
            m_dump_data.assert_called_with(storage_data)
            self.assertEqual(response, dumped_data)
            self.assertEqual(code, 200)

    def test_patch_bad_request(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data'
        ]) as mocked:
            mocked[-1].side_effect = BadRequest
            response, code = MyResource().patch(1)
            self.assertEqual({'error': BadRequest.description}, response)
            self.assertEqual(400, code)

    def test_patch_load_error(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data'
        ]) as mocked:
            errors = [{'error': 'Test error'}]
            mocked[-1].side_effect = LoadError(errors)
            response, code = MyResource().patch(1)
            self.assertEqual(errors, response)
            self.assertEqual(400, code)

    def test_patch_does_not_exist(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data',
            'app.common.services.base_storage.BaseStorage.update'
        ]) as mocked:
            mocked[-1].side_effect = DoesNotExist(1)
            response, code = MyResource().patch(1)
            self.assertEqual({'error': DoesNotExist.description}, response)
            self.assertEqual(404, code)

    def test_patch_already_exists(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data',
            'app.common.services.base_storage.BaseStorage.update',
            'app.common.services.base_storage.BaseStorage.commit'
        ]) as mocked:
            mocked[-1].side_effect = AlreadyExists
            response, code = MyResource().patch(1)
            self.assertEqual({'error': AlreadyExists.description}, response)
            self.assertEqual(409, code)

    def test_patch(self):
        with patch_multiple([
            'app.common.api.base_resource.request',
            'app.common.serializers.base_serializer.BaseSerializer.load_data',
            'app.common.services.base_storage.BaseStorage.update',
            'app.common.services.base_storage.BaseStorage.commit',
            'app.common.serializers.base_serializer.BaseSerializer.dump_data'
        ]) as (m_request, m_load_data, m_create, m_commit, m_dump_data):
            data = m_request.get_json.return_value = {'a': 1, 'b': 2, 'c': 3}
            validated_data = m_load_data.return_value = {'a': 1, 'b': 2}
            storage_data = m_create.return_value = MagicMock()
            dumped_data = m_dump_data.return_value = {'id': 1, 'a': 1, 'b': 2}
            response, code = MyResource().patch(1)
            m_load_data.assert_called_with(partial=True, id=1, **data)
            m_create.assert_called_with(**validated_data)
            m_commit.assert_called_with()
            m_dump_data.assert_called_with(storage_data)
            self.assertEqual(response, dumped_data)
            self.assertEqual(code, 200)

    def test_delete_does_not_exist(self):
        with patch(
            'app.common.services.base_storage.BaseStorage.delete',
            side_effect=DoesNotExist(1)
        ):
            response, code = MyResource().delete(1)
            self.assertEqual({'error': DoesNotExist.description}, response)
            self.assertEqual(404, code)

    def test_delete(self):
        with patch_multiple([
            'app.common.services.base_storage.BaseStorage.delete',
            'app.common.services.base_storage.BaseStorage.commit'
        ]) as (m_delete, m_commit):
            response, code = MyResource().delete(1)
            m_delete.assert_called_with(1)
            m_commit.assert_called_with()
            self.assertEqual(response, {})
            self.assertEqual(code, 204)


if __name__ == '__main__':
    unittest.main()
