# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from flask_restful import Api as FlaskApi


class BaseApi(FlaskApi):
    """
    Base application API.
    API_RESOURCES should be list of tuples like: [
        (MyResource, ('/my-resources', '/my-resources/<int:id>')),
    ]
    """

    PREFIX = '/api'
    API_RESOURCES = []

    def __init__(self, app=None, prefix=None, *args, **kwargs):
        self.PREFIX = self.__class__.PREFIX
        self.API_RESOURCES = self.__class__.API_RESOURCES
        super(BaseApi, self).__init__(app=app, prefix=prefix or self.PREFIX,
                                      *args, **kwargs)

    @classmethod
    def _get_endpoint_prefix(cls):
        return cls.PREFIX.replace('/', '_').strip('_')

    def register_resources(self):
        endpoint_prefix = self._get_endpoint_prefix()
        for resource, path in self.API_RESOURCES:
            resource_endpoint = str('{prefix}__{resourse}'.format(
                prefix=endpoint_prefix,
                resourse=resource.get_endpoint()
            ))
            if isinstance(path, (list, tuple)):
                self.add_resource(resource, *path, endpoint=resource_endpoint)
            else:
                self.add_resource(resource, path, endpoint=resource_endpoint)

    def init_app(self, app):
        self.register_resources()
        super(BaseApi, self).init_app(app)
