# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from copy import deepcopy

from flask import request
from flask_restful import Resource
from werkzeug.exceptions import BadRequest

from app.common.serializers import BaseSerializer, FilterSerializer
from app.common.serializers.exceptions import LoadError
from app.common.services import BaseStorage
from app.common.services.storage_exceptions import DoesNotExist, AlreadyExists
from app.common.utils import decapitalize


class BaseResource(Resource):
    """
    Base API Resource.
    Allowed methods: get, post, put, patch, delete
    """

    SERIALIZER = BaseSerializer
    FILTER_SERIALIZER = FilterSerializer
    STORAGE = BaseStorage

    def __init__(self):
        super(BaseResource, self).__init__()

        self.SERIALIZER = self.__class__.SERIALIZER
        if not issubclass(self.SERIALIZER, BaseSerializer):
            raise TypeError('Not a serializer:', self.SERIALIZER)
        self.serializer = self.SERIALIZER()

        self.FILTER_SERIALIZER = self.__class__.FILTER_SERIALIZER
        if not issubclass(self.FILTER_SERIALIZER, FilterSerializer):
            raise TypeError('Not a filter serializer:', self.FILTER_SERIALIZER)
        self.filter_serializer = self.FILTER_SERIALIZER(self.SERIALIZER)

        self.STORAGE = self.__class__.STORAGE
        if not issubclass(self.STORAGE, BaseStorage):
            raise TypeError('Not a storage:', self.STORAGE)
        self.storage = self.STORAGE()

    @classmethod
    def get_endpoint(cls):
        return decapitalize(cls.__name__)

    def get(self, id=None):
        try:
            if id is not None:
                storage_data = self.storage.read(id)
                result = self.serializer.dump_data(storage_data)
            else:
                options = self.filter_serializer.load_data(
                    **request.args.to_dict())
                storage_data = self.storage.read(
                    filter=options['filter'],
                    paging=options['paging']
                )
                total = self.storage.count(filter=options['filter'])
                result = deepcopy(options)
                result['data'] = self.serializer.dump_data(storage_data)
                result['paging']['total'] = total
            return result, 200
        except LoadError as e:
            return e.errors, 400
        except DoesNotExist as e:
            return {'error': e.description}, 404

    def post(self):
        try:
            data = deepcopy(request.get_json()) or {}
            validated_data = self.serializer.load_data(
                partial=False, **data)
            storage_data = self.storage.create(**validated_data)
            self.storage.commit()
            result = self.serializer.dump_data(storage_data)
            return result, 201
        except BadRequest as e:
            return {'error': e.description}, 400
        except LoadError as e:
            return e.errors, 400
        except AlreadyExists as e:
            return {'error': e.description}, 409

    def put(self, id):
        return self._update(id)

    def patch(self, id):
        return self._update(id, partial=True)

    def _update(self, id, partial=False):
        try:
            data = deepcopy(request.get_json()) or {}
            data['id'] = id
            validated_data = self.serializer.load_data(
                partial=partial, **data)
            storage_data = self.storage.update(**validated_data)
            self.storage.commit()
            result = self.serializer.dump_data(storage_data)
            return result, 200
        except BadRequest as e:
            return {'error': e.description}, 400
        except LoadError as e:
            return e.errors, 400
        except DoesNotExist as e:
            return {'error': e.description}, 404
        except AlreadyExists as e:
            return {'error': e.description}, 409

    def delete(self, id):
        try:
            self.storage.delete(id)
            self.storage.commit()
            return {}, 204
        except DoesNotExist as e:
            return {'error': e.description}, 404
