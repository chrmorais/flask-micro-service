# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.utils.singleton import Singleton as singleton
from app.common.utils.strings import decapitalize, plural
from app.common.utils.asserts import AssertsMixin
from app.common.utils.mocks import patch_multiple


__all__ = [
    'singleton',
    'decapitalize',
    'plural',
    'AssertsMixin',
    'patch_multiple',
]
