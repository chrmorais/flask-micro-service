# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from app.common.utils import singleton


class TestSingleton(unittest.TestCase):
    """
    Test singleton decorator
    """

    @singleton
    class TestClass(object):
        test_field = 0

        def __init__(self, value):
            self.test_field = value

    def test_singleton(self):
        """
        Create two instances of TestClass.
        Check that the second instance is link to the first instance.
        """
        obj1 = self.TestClass('A')
        obj2 = self.TestClass('B')
        self.assertEqual(obj1, obj2)
        self.assertEqual('A', obj1.test_field)
        self.assertEqual('A', obj2.test_field)
        obj2.test_field = 'C'
        self.assertEqual('C', obj1.test_field)
        self.assertEqual('C', obj2.test_field)


if __name__ == '__main__':
    unittest.main()
