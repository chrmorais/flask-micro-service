# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from app.common.utils import decapitalize, plural


class TestStrings(unittest.TestCase):

    def test_decapitalize(self):
        self.assertEqual(decapitalize('OneTwoThree'), 'one_two_three')
        self.assertEqual(decapitalize('OneTwo_Three'), 'one_two_three')
        self.assertEqual(decapitalize('One_TwoThree'), 'one_two_three')
        self.assertEqual(decapitalize('One_Two_Three'), 'one_two_three')
        self.assertEqual(decapitalize('one_two_three'), 'one_two_three')
        self.assertEqual(decapitalize('1TwoThree'), '1_two_three')
        self.assertEqual(decapitalize('One2Three'), 'one_2_three')
        self.assertEqual(decapitalize('OneTwo3'), 'one_two_3')
        self.assertEqual(decapitalize('12Three'), '12_three')
        self.assertEqual(decapitalize('One23'), 'one_23')
        self.assertEqual(decapitalize('123'), '123')

    def test_plural(self):
        self.assertEqual(plural('book'), 'books')
        self.assertEqual(plural('voice'), 'voices')
        self.assertEqual(plural('bass'), 'basses')
        self.assertEqual(plural('box'), 'boxes')
        self.assertEqual(plural('bash'), 'bashes')
        self.assertEqual(plural('batch'), 'batches')
        self.assertEqual(plural('hero'), 'heroes')
        self.assertEqual(plural('body'), 'bodies')
        self.assertEqual(plural('fly'), 'flies')
        self.assertEqual(plural('day'), 'days')
        self.assertEqual(plural('grey'), 'greys')
        self.assertEqual(plural('diy'), 'diys')
        self.assertEqual(plural('boy'), 'boys')
        self.assertEqual(plural('guy'), 'guys')
        self.assertEqual(plural('wolf'), 'wolves')
        self.assertEqual(plural('life'), 'lives')


if __name__ == '__main__':
    unittest.main()
