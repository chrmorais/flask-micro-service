# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from app.common.utils import AssertsMixin


class TestAssertsMixin(AssertsMixin, unittest.TestCase):

    def test_raise(self):
        """
        Context of assertRaisesDetails should be raised exception.
        """
        with self.assertRaisesDetails(
            ZeroDivisionError, lambda x: 1/x, x=0
        ) as e:
            self.assertIsInstance(e, ZeroDivisionError)

    @unittest.expectedFailure
    def test_not_raises(self):
        """
        We should fail with AssertionError: "ZeroDivisionError not raised".
        """
        self.assertRaisesDetails(ZeroDivisionError, lambda x: 1/x, x=1)

    @unittest.expectedFailure
    def test_raises_on_exit(self):
        """
        We should not ignore any errors that happened in context.
        """
        with self.assertRaisesDetails(
            ZeroDivisionError, lambda x: 1/x, x=0
        ):
            self.fail('Test should fail when exception raises here.')


if __name__ == '__main__':
    unittest.main()
