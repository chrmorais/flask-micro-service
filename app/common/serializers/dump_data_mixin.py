# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from marshmallow import Schema

from app.common.serializers.exceptions import DumpError


class DumpDataMixin(Schema):

    def dump_data(self, obj, **kwargs):
        result = self.dump(obj, **kwargs)
        if result.errors:
            raise DumpError(result.errors)
        return result.data
