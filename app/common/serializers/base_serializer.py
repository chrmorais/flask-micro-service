# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from marshmallow import Schema, fields, utils
from marshmallow.validate import Range

from app.common.serializers.load_data_mixin import LoadDataMixin
from app.common.serializers.dump_data_mixin import DumpDataMixin
from app.common.models import BaseModel


class BaseSerializer(LoadDataMixin, DumpDataMixin, Schema):
    id = fields.Integer(validate=Range(min=1))

    def _is_many(self, obj):
        return utils.is_collection(obj) and not isinstance(obj, BaseModel)

    def dump(self, obj, **kwargs):
        kwargs['many'] = self._is_many(obj)
        return super(BaseSerializer, self).dump(obj, **kwargs)
