# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from marshmallow import Schema, fields
from marshmallow.validate import Range

from app.common.serializers.load_data_mixin import LoadDataMixin


class PagingSerializer(LoadDataMixin, Schema):
    limit = fields.Integer(default=1024, validate=Range(min=1, max=1024))
    offset = fields.Integer(default=0, validate=Range(min=0))
    total = fields.Integer(dump_only=True)

    def __init__(self, *args, **kwargs):
        super(PagingSerializer, self).__init__(missed=True, *args, **kwargs)

    def load(self, data, **kwargs):
        kwargs['many'] = False
        kwargs['partial'] = False
        return super(PagingSerializer, self).load(data, **kwargs)
