# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from marshmallow import fields

from app.common.utils import AssertsMixin
from app.common.serializers import BaseSerializer
from app.common.serializers.exceptions import LoadError
from app.common.models import BaseModel


class MySerializer(BaseSerializer):
    my_field = fields.String(required=True)


class MyModel(BaseModel):
    my_field = None


TEST_STR_1 = 'The rascals, perhaps, may betake them to robbing'
TEST_STR_2 = 'The dogs to be sure have got nothing to eat'


class TestBaseSerializer(AssertsMixin, unittest.TestCase):

    def setUp(self):
        self.serializer = MySerializer()

    def test_load_data(self):
        data = self.serializer.load_data(id=1, my_field=TEST_STR_1)
        self.assertEqual({'id': 1, 'my_field': TEST_STR_1}, data)

    def test_load_data_ignore_others(self):
        data = self.serializer.load_data(
            id=1, my_field=TEST_STR_1, another_field=TEST_STR_2)
        self.assertEqual({'id': 1, 'my_field': TEST_STR_1}, data)

    def test_load_data_no_id(self):
        data = self.serializer.load_data(my_field=TEST_STR_1)
        self.assertEqual({'my_field': TEST_STR_1}, data)

    def test_load_data_id_should_be_positive(self):
        with self.assertRaisesDetails(
            LoadError,
            self.serializer.load_data, id=0, my_field=TEST_STR_1
        ) as e:
            self.assertEqual({
                'id': ['Must be at least 1.']
            }, e.errors)

    def test_load_data_empty_required(self):
        with self.assertRaisesDetails(
            LoadError,
            self.serializer.load_data, id=1
        ) as e:
            self.assertEqual({
                'my_field': ['Missing data for required field.']
            }, e.errors)

    def test_load_data_partial(self):
        data = self.serializer.load_data(partial=True, id=1)
        self.assertEqual({'id': 1}, data)

    def test_dump_data_one(self):
        model_item = MyModel(id=1, my_field=TEST_STR_1)
        data = self.serializer.dump_data(model_item)
        self.assertEqual({'id': 1, 'my_field': TEST_STR_1}, data)

    def test_dump_data_many(self):
        model_list = [
            MyModel(id=1, my_field=TEST_STR_1),
            MyModel(id=2, my_field=TEST_STR_2)
        ]
        data = self.serializer.dump_data(model_list)
        self.assertEqual([
            {'id': 1, 'my_field': TEST_STR_1},
            {'id': 2, 'my_field': TEST_STR_2}
        ], data)

    def test_dump_data_ignore_others(self):

        class Another(BaseModel):
            my_field = None
            another_field = None

        model_item = Another(id=1, my_field=TEST_STR_1,
                             another_field=TEST_STR_2)
        data = self.serializer.dump_data(model_item)
        self.assertEqual({'id': 1, 'my_field': TEST_STR_1}, data)


if __name__ == '__main__':
    unittest.main()
