# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from app.common.utils import AssertsMixin
from app.common.serializers import PagingSerializer
from app.common.serializers.exceptions import LoadError


class TestPagingSerializer(AssertsMixin, unittest.TestCase):

    def setUp(self):
        self.serializer = PagingSerializer()

    def test_load_data_default(self):
        data = self.serializer.load_data()
        self.assertEqual({'limit': 1024, 'offset': 0}, data)

    def test_load_data(self):
        data = self.serializer.load_data(limit=256, offset=512)
        self.assertEqual({'limit': 256, 'offset': 512}, data)

    def test_validate_limit_min_value(self):
        with self.assertRaisesDetails(
            LoadError,
            self.serializer.load_data, limit=0
        ) as e:
            self.assertEqual({
                'limit': ['Must be between 1 and 1024.']
            }, e.errors)

    def test_validate_limit_max_value(self):
        with self.assertRaisesDetails(
            LoadError,
            self.serializer.load_data, limit=1025
        ) as e:
            self.assertEqual({
                'limit': ['Must be between 1 and 1024.']
            }, e.errors)

    def test_validate_offset_min_value(self):
        with self.assertRaisesDetails(
            LoadError,
            self.serializer.load_data, offset=-1
        ) as e:
            self.assertEqual({
                'offset': ['Must be at least 0.']
            }, e.errors)
