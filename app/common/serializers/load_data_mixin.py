# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from marshmallow import Schema, post_load

from app.common.serializers.exceptions import LoadError


class LoadDataMixin(Schema):

    def __init__(self, missed=False, *args, **kwargs):
        super(LoadDataMixin, self).__init__(*args, **kwargs)
        self.missed = missed

    @post_load
    def _include_missed(self, data):
        if self.missed:
            data.update(dict([
                (attr_name, self.fields[attr_name].default)
                for attr_name in set(self.fields) - set(data)
                if (not self.fields[attr_name].dump_only and
                    self.fields[attr_name].default is not None)
            ]))
        return data

    def load_data(self, many=None, partial=None, **data):
        result = self.load(data, many=many, partial=partial)
        if result.errors:
            raise LoadError(result.errors)
        return result.data
