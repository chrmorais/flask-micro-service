# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class SerializerError(Exception):
    def __init__(self, errors):
        super(SerializerError, self).__init__()
        self.errors = errors


class LoadError(SerializerError):
    pass


class DumpError(SerializerError):
    pass
