# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from marshmallow import Schema, fields

from app.common.serializers.load_data_mixin import LoadDataMixin
from app.common.serializers.paging_serializer import PagingSerializer


class FilterSerializer(LoadDataMixin, Schema):
    paging = fields.Nested(PagingSerializer, many=False)
    filter = fields.Nested(Schema, many=False, partial=True)

    def __init__(self, serializer_cls, *args, **kwargs):
        super(FilterSerializer, self).__init__(*args, **kwargs)
        self.declared_fields['filter'].nested = serializer_cls
        self.fields['filter'].nested = serializer_cls(many=False, partial=True)

    def load(self, data, **kwargs):
        data['paging'] = data
        data['filter'] = data
        kwargs['many'] = False
        kwargs['partial'] = False
        return super(FilterSerializer, self).load(data, **kwargs)
