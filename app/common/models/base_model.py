# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class BaseModel(object):
    """
    Base abstract model class
    """
    id = None

    def __init__(self, **attrs):
        super(BaseModel, self).__init__()
        for attr_name, attr_value in attrs.items():
            if hasattr(self, attr_name):
                setattr(self, attr_name, attr_value)

    def __int__(self):
        return self.id

    def __repr__(self):
        return '<{class_name}:{id}>'.format(
            class_name=self.__class__.__name__,
            id=self.id
        )

    def __eq__(self, other):
        if type(self) != type(other):  # pylint: disable=unidiomatic-typecheck
            return False
        for attr_name in self.__dict__:
            if (not attr_name.startswith('_') and
                    getattr(self, attr_name) != getattr(other, attr_name)):
                return False
        return True

    def __ne__(self, other):
        return not self.__eq__(other)
