# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from app.database.sql_alchemy import db
from app.common.models import SQLAlchemyModel


class MyFirstModel(SQLAlchemyModel):
    __bind_key__ = 'test'
    my_field = db.Column(db.Text)


class MySecondModel(SQLAlchemyModel):
    __bind_key__ = 'test'
    __tablename__ = 'my_custom_table'
    my_field = db.Column(db.Text)


class TestSQLAlchemyModel(unittest.TestCase):
    """
    Declare test class MyModel based on SQLAlchemyModel.
    Check main attributes.
    """

    def test_default_table_name(self):
        self.assertEqual('my_first_models', MyFirstModel.__tablename__)

    def test_custom_table_name(self):
        self.assertEqual('my_custom_table', MySecondModel.__tablename__)

    def test_has_id_field(self):
        self.assertTrue(hasattr(MyFirstModel, 'id'))

    def test_has_custom_field(self):
        self.assertTrue(hasattr(MyFirstModel, 'my_field'))


if __name__ == '__main__':
    unittest.main()
