# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from app.common.models import BaseModel


class MyModel(BaseModel):
    my_field = None


TEST_STRING_1 = 'Those villains, the weavers, are all grown refractory'
TEST_STRING_2 = 'So hang them in clusters round each Manufactory'


class TestBaseModel(unittest.TestCase):
    """
    Declare test class MyModel based on BaseModel.
    Check main attributes,
    check int, str representation,
    check custom equal method.
    """

    def test_has_id_field(self):
        self.assertTrue(hasattr(MyModel, 'id'))

    def test_has_custom_field(self):
        self.assertTrue(hasattr(MyModel, 'my_field'))

    def test_constructor(self):
        model = MyModel(id=42, my_field=TEST_STRING_1)
        self.assertEqual(42, model.id)
        self.assertEqual(TEST_STRING_1, model.my_field)

    def test_int(self):
        model = MyModel(id=42, my_field=TEST_STRING_1)
        self.assertEqual(42, int(model))

    def test_str(self):
        model = MyModel(id=42, my_field=TEST_STRING_1)
        self.assertEqual('<MyModel:42>', str(model))

    def test_equal(self):
        model1 = MyModel(id=42, my_field=TEST_STRING_1)
        model2 = MyModel(id=42, my_field=TEST_STRING_1)
        self.assertTrue(model1 == model2)
        self.assertFalse(model1 != model2)

    def test_not_equal_type(self):
        class Another(BaseModel):
            my_field = None

        model1 = MyModel(id=42, my_field=TEST_STRING_1)
        model2 = Another(id=42, my_field=TEST_STRING_1)
        self.assertFalse(model1 == model2)
        self.assertTrue(model1 != model2)

    def test_not_equal_id(self):
        model1 = MyModel(id=42, my_field=TEST_STRING_1)
        model2 = MyModel(id=43, my_field=TEST_STRING_1)
        self.assertFalse(model1 == model2)
        self.assertTrue(model1 != model2)

    def test_not_equal_custom_field(self):
        model1 = MyModel(id=42, my_field=TEST_STRING_1)
        model2 = MyModel(id=42, my_field=TEST_STRING_2)
        self.assertFalse(model1 == model2)
        self.assertTrue(model1 != model2)


if __name__ == '__main__':
    unittest.main()
