# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest
from mock import patch

from sqlalchemy.exc import SQLAlchemyError, IntegrityError

from app.tests import TestCase
from app.database.sql_alchemy import db
from app.common.models import BaseModel, SQLAlchemyModel
from app.common.services import SQLAlchemyStorage
from app.common.services.storage_exceptions import DoesNotExist, AlreadyExists
from app.common.utils import patch_multiple


class MySQLAlchemyModel(SQLAlchemyModel):
    __bind_key__ = 'test'
    my_field = db.Column(db.String, unique=True)


class MyStorage(SQLAlchemyStorage):
    MODEL = MySQLAlchemyModel


TEST_STR_1 = 'Men are more easily made than machinery'
TEST_STR_2 = 'Stockings fetch better prices than lives'
TEST_STR_3 = 'Showing how Commerce, how Liberty thrives!'


class TestSQLAlchemyStorage(TestCase):

    def setUp(self):
        self.storage = MyStorage()
        db.session.add(MySQLAlchemyModel(id=1, my_field=TEST_STR_1))
        db.session.add(MySQLAlchemyModel(id=2, my_field=TEST_STR_2))
        db.session.commit()

    def tearDown(self):
        db.session.query(MySQLAlchemyModel).delete()
        db.session.commit()

    def test_model_type_validation(self):

        class BadModel(BaseModel):
            pass

        class BadStorage(SQLAlchemyStorage):
            MODEL = BadModel

        with self.assertRaisesDetails(TypeError, BadStorage) as e:
            self.assertEqual('Not a model:', e.args[0])
            self.assertEqual(BadModel, e.args[1])

    def test_commit(self):
        self.storage.update(id=1, my_field=TEST_STR_3)
        self.storage.commit()
        model = self.storage.read(id=1)
        self.assertEqual(TEST_STR_3, model.my_field)

    def test_commit_isolated(self):
        with patch('app.common.services.sql_alchemy_storage'
                   '.db.session.commit') as mocked:
            self.storage.commit()
            mocked.assert_called_once_with()

    def test_commit_unexpected_error(self):
        with patch_multiple([
            'app.common.services.sql_alchemy_storage.db.session.commit',
            'app.common.services.sql_alchemy_storage.db.session.rollback',
        ]) as (m_commit, m_rollback):
            m_commit.side_effect = SQLAlchemyError
            self.assertRaises(SQLAlchemyError, self.storage.commit)
            m_rollback.assert_called_once_with()

    def test_commit_integrity_error(self):
        with patch_multiple([
            'app.common.services.sql_alchemy_storage.db.session.commit',
            'app.common.services.sql_alchemy_storage.db.session.rollback',
        ]) as (m_commit, m_rollback):
            m_commit.side_effect = IntegrityError(None, None, None)
            self.assertRaises(IntegrityError, self.storage.commit)
            m_rollback.assert_called_once_with()

    def test_rollback(self):
        self.storage.update(id=1, my_field=TEST_STR_3)
        self.storage.rollback()
        model = self.storage.read(id=1)
        self.assertEqual(TEST_STR_1, model.my_field)

    def test_rollback_isolated(self):
        with patch('app.common.services.sql_alchemy_storage'
                   '.db.session.rollback') as mocked:
            self.storage.rollback()
            mocked.assert_called_once_with()

    def test_count(self):
        total = self.storage.count()
        self.assertEqual(2, total)

    def test_read_one(self):
        model = self.storage.read(id=1)
        self.assertIsInstance(model, MySQLAlchemyModel)
        self.assertEqual(1, model.id)
        self.assertEqual(TEST_STR_1, model.my_field)

    def test_read_many(self):
        models = self.storage.read()
        self.assertIsInstance(models, list)
        self.assertEqual(2, len(models))
        self.assertIn(MySQLAlchemyModel(id=1, my_field=TEST_STR_1), models)
        self.assertIn(MySQLAlchemyModel(id=2, my_field=TEST_STR_2), models)

    def test_read_many_paging(self):
        models = self.storage.read(filter={}, paging={'limit': 1, 'offset': 1})
        self.assertIsInstance(models, list)
        self.assertEqual(1, len(models))
        self.assertIn(MySQLAlchemyModel(id=2, my_field=TEST_STR_2), models)

    def test_read_does_not_exist(self):
        with self.assertRaisesDetails(
            DoesNotExist,
            self.storage.read, id=3
        ) as e:
            self.assertEqual(3, e.id)

    def test_create(self):
        model = self.storage.create(my_field=TEST_STR_3)
        self.storage.commit()
        self.assertIsInstance(model, MySQLAlchemyModel)
        self.assertEqual(3, model.id)
        self.assertEqual(TEST_STR_3, model.my_field)

    def test_create_already_exists(self):
        self.storage.create(my_field=TEST_STR_1)
        self.assertRaises(AlreadyExists, self.storage.commit)

    def test_create_unexpected_error(self):
        with patch_multiple([
            'app.common.services.sql_alchemy_storage.db.session.add',
            'app.common.services.sql_alchemy_storage.db.session.rollback',
        ]) as (m_add, m_rollback):
            m_add.side_effect = SQLAlchemyError
            self.assertRaises(
                SQLAlchemyError,
                self.storage.create, my_field=TEST_STR_3
            )
            m_rollback.assert_called_once_with()

    def test_update(self):
        model = self.storage.update(id=1, my_field=TEST_STR_3)
        self.storage.commit()
        self.assertIsInstance(model, MySQLAlchemyModel)
        self.assertEqual(1, model.id)
        self.assertEqual(TEST_STR_3, model.my_field)

    def test_update_does_not_exist(self):
        with self.assertRaisesDetails(
            DoesNotExist,
            self.storage.update, id=3, my_field=TEST_STR_3
        ) as e:
            self.assertEqual(3, e.id)

    def test_update_already_exists(self):
        self.storage.update(id=2, my_field=TEST_STR_1)
        self.assertRaises(AlreadyExists, self.storage.commit)

    def test_update_unexpected_error(self):
        with patch_multiple([
            'app.common.services.sql_alchemy_storage.db.session.add',
            'app.common.services.sql_alchemy_storage.db.session.rollback',
        ]) as (m_add, m_rollback):
            m_add.side_effect = SQLAlchemyError
            self.assertRaises(
                SQLAlchemyError,
                self.storage.update, id=1, my_field=TEST_STR_3
            )
            m_rollback.assert_called_once_with()

    def test_delete(self):
        resul = self.storage.delete(id=1)
        self.storage.commit()
        self.assertIsNone(resul)

    def test_delete_does_not_exist(self):
        with self.assertRaisesDetails(
            DoesNotExist,
            self.storage.delete, id=3
        ) as e:
            self.assertEqual(3, e.id)

    def test_delete_unexpected_error(self):
        with patch_multiple([
            'app.common.services.sql_alchemy_storage.db.session.delete',
            'app.common.services.sql_alchemy_storage.db.session.rollback',
        ]) as (m_delete, m_rollback):
            m_delete.side_effect = SQLAlchemyError
            self.assertRaises(
                SQLAlchemyError,
                self.storage.delete, id=1
            )
            m_rollback.assert_called_once_with()


if __name__ == '__main__':
    unittest.main()
