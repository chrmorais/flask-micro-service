# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class StorageException(Exception):
    description = 'Storage error'


class DoesNotExist(StorageException):
    description = 'Resource does not exist'

    def __init__(self, id, *args, **kwargs):
        super(DoesNotExist, self).__init__(*args, **kwargs)
        self.id = id


class AlreadyExists(StorageException):
    description = 'Resource already exists'
