# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from sqlalchemy.exc import SQLAlchemyError, IntegrityError

from app.database.sql_alchemy import db
from app.common.models import SQLAlchemyModel
from app.common.services.base_storage import BaseStorage
from app.common.services.storage_exceptions import DoesNotExist, AlreadyExists


class SQLAlchemyStorage(BaseStorage):
    MODEL = SQLAlchemyModel

    def __init__(self):
        super(SQLAlchemyStorage, self).__init__()
        if not issubclass(self.MODEL, SQLAlchemyModel):
            raise TypeError('Not a model:', self.MODEL)
        self.session = db.session

    def commit(self):
        try:
            self.session.commit()
        except IntegrityError as e:
            self.session.rollback()
            if any([msg in str(e).lower()
                    for msg in ('duplicate', 'unique')]):
                raise AlreadyExists(*e.args)
            raise
        except SQLAlchemyError:
            self.session.rollback()
            raise

    def rollback(self):
        self.session.rollback()

    def _get_query(self, filter=None):
        query = self.session.query(self.MODEL)
        if filter:
            query = query.filter_by(**filter)
        return query

    def _get_item(self, id):
        result = self._get_query().get(id)
        if not result:
            raise DoesNotExist(id)
        return result

    def _get_list(self, filter=None, paging=None):
        query = self._get_query(filter=filter)
        if paging:
            query = query.limit(paging['limit'])
            query = query.offset(paging['offset'])
        return query.all()

    def _get_count(self, filter=None):
        query = self._get_query(filter=filter)
        return query.count()

    def _save(self, model):
        try:
            self.session.add(model)
        except SQLAlchemyError:
            self.session.rollback()
            raise

    def _remove(self, model):
        try:
            self.session.delete(model)
        except SQLAlchemyError:
            self.session.rollback()
            raise
