#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from flask_script import Manager, Shell, Server
from flask_migrate import MigrateCommand

from app import Application


app = Application(config_name=os.getenv('CONFIG'))
manager = Manager(app)

# Make migration commands:
#   python manage.py migrations --help
#   python manage.py migrations init [--multidb]
#   python manage.py migrations revision
manager.add_command('migrations', MigrateCommand)

# Make shell context
manager.add_command('shell', Shell(
    make_context=lambda: dict(app=app)
))

# Run local server
manager.add_command('run', Server(
    host=app.config['HOST'],
    port=app.config['PORT']
))


if __name__ == '__main__':
    manager.run()
